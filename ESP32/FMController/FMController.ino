#include <AccelStepper.h>
#include "analogWrite.h"
#include "FMControllerMethods.h"

// Creates an instance
AccelStepper myStepper(1, 12, 13);
FMControllerMethods fm(34, 35, 32, 4, 16, 17, 33, 12, 13, 5, 14);

// Calibrate will find this value
int maxSteps = 0;
int originalMaxSteps = 0;

void setup()
{
  Serial.begin(115200);
  pinMode(fm.getButton1(), INPUT);
  pinMode(fm.getButton2(), INPUT);
  pinMode(fm.getButton3(), INPUT);
  pinMode(fm.getRedPin(), OUTPUT);
  pinMode(fm.getGreenPin(), OUTPUT);
  pinMode(fm.getBluePin(), OUTPUT);
  pinMode(fm.getStepPin(), OUTPUT);
  pinMode(fm.getDirPin(), OUTPUT);
  pinMode(fm.getLimitBackPin(), OUTPUT);
  pinMode(fm.getLimitFrontPin(), OUTPUT);

  digitalWrite(fm.getStepPin(), LOW);
  digitalWrite(fm.getDirPin(), HIGH); // Low = forward, high = backwards
  fm.setColor(0, 0, 0);
  
  originalMaxSteps = fm.calibrate();
  maxSteps = originalMaxSteps;
  if (maxSteps > 0)
  {
    fm.setColor(0, 255, 0);

    // set the maximum speed, acceleration factor,
    // initial speed and the target position
    myStepper.setMaxSpeed(10000);
    myStepper.setAcceleration(2000);
    //myStepper.setSpeed(200);
    myStepper.moveTo(fm.getMaxSteps() * -1);
  }
  else
  {
    fm.setColor(255, 0, 0);
    while (true)
    {
      delay(1000);
    }
  }
}

bool stopFM = false;

void loop() {
  int fmSpeed = fm.getSpeed();
  
  if (digitalRead(fm.getButton3()) == HIGH)
  {
    fm.setMaxSteps(abs(myStepper.currentPosition()));
    myStepper.moveTo(fm.getMaxSteps() * -1);
  }
  if (digitalRead(fm.getButton1()) == HIGH && fmSpeed == 0)
  {
    fm.setMaxSteps(fm.getOriginalMaxSteps());
    fm.setColor(0, 0, 255);
    delay(500);
    fm.setColor(0, 255, 0);
  }
  
  if (digitalRead(fm.getLimitFrontPin()) == HIGH)
  {
    myStepper.setCurrentPosition(fm.getMaxSteps() * -1);
  }
  if (digitalRead(fm.getLimitBackPin()) == HIGH)
  {
    myStepper.setCurrentPosition(0);
  }
  
  
  if (digitalRead(fm.getButton2())) // STOP
  {
    stopFM = !stopFM;
    if (stopFM)
    {
      fm.setColor(255, 0, 0); //Red
    }
    if (stopFM == false) {
      fm.setColor(0, 255, 0); // Green
    }
    // Waiting for button to be released
    while (digitalRead(fm.getButton2())) delay(500);
  }
  //int speedValue = potValue / 100;
  //int speedValue = fmSpeed;
  
  if (stopFM == false)
  {
    if (fmSpeed > 0)
    {
      myStepper.setAcceleration(fmSpeed);
      
      if (myStepper.distanceToGo() == 0)
      {
        //myStepper.moveTo(-myStepper.currentPosition());
        if (myStepper.currentPosition() < 0)
        {
          myStepper.moveTo(0);
        }
        else
        {
          myStepper.moveTo(fm.getMaxSteps() * -1);
        }
      }
    
      // Move the motor one step
      myStepper.run();
    }
  }
}
