#ifndef _FM_CONTROLLER_METHODS_
#define _FM_CONTROLLER_METHODS_

class FMControllerMethods
{
  private:
    int button1;
    int button2;
    int button3;
    int redPin;
    int greenPin;
    int bluePin;
    int potPin;
    int stepPin;
    int dirPin;
    int limitBackPin;
    int limitFrontPin;
    int maxSteps;
    int originalMaxSteps;
    int currentPotValue;
  public:
    FMControllerMethods(int button1, int button2, int button3, int redPin, int greenPin, int bluePin, int speedPot, int stepPin, int dirPin, int limitBackPin, int limitFrontPin);
    void setColor(int red, int green, int blue);
    bool calibrate();
    int getButton1();
    int getButton2();
    int getButton3();
    int getRedPin();
    int getGreenPin();
    int getBluePin();
    int getStepPin();
    int getDirPin();
    int getLimitBackPin();
    int getLimitFrontPin();
    int getMaxSteps();
    void setMaxSteps(int newMaxSteps);
    int getOriginalMaxSteps();
    int getSpeed();
};

#endif
