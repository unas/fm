#include "FMControllerMethods.h"
#include "analogWrite.h"

FMControllerMethods::FMControllerMethods(int button1, int button2, int button3, int redPin, int greenPin, int bluePin, int potPin, int stepPin, int dirPin, int limitBackPin, int limitFrontPin)
{
  this->button1 = button1;
  this->button2 = button2;
  this->button3 = button3;
  this->redPin = redPin;
  this->greenPin = greenPin;
  this->bluePin = bluePin;
  this->potPin = potPin;
  this->stepPin = stepPin;
  this->dirPin = dirPin;
  this->limitBackPin = limitBackPin;
  this->limitFrontPin = limitFrontPin;
}

void FMControllerMethods::setColor(int red, int green, int blue)
{
  analogWrite(this->redPin, red);
  analogWrite(this->greenPin, green);
  analogWrite(this->bluePin, blue);
}

bool FMControllerMethods::calibrate()
{

  this->maxSteps = 0;
  this->setColor(255, 255, 0);

  // Going forward until finding limit
  digitalWrite(this->dirPin, HIGH);
  boolean startCount = false;
  while (true)
  {
    digitalWrite(this->stepPin, HIGH);
    delayMicroseconds(3000);
    digitalWrite(this->stepPin, LOW);
    delayMicroseconds(3000);

    if (startCount == false && digitalRead(this->limitBackPin) == HIGH)
    {
      // Going backwards until we find the limit
      digitalWrite(this->dirPin, LOW);
      startCount = true;
    }
    
    if (startCount && digitalRead(this->limitFrontPin) == HIGH)
    {
      // Limits found. Moving 3 steps forward and removing 6 from max count
      digitalWrite(this->dirPin, HIGH);
      for(int i = 0; i < 10; ++i) {
        digitalWrite(this->stepPin, HIGH);
        delayMicroseconds(3000);
        digitalWrite(this->stepPin, LOW);
        delayMicroseconds(3000);
      }
      this->maxSteps -= 20;

      // Moving to back
      for(int i = 0; i < this->maxSteps; ++i)
      {
        digitalWrite(this->stepPin, HIGH);
        delayMicroseconds(3000);
        digitalWrite(this->stepPin, LOW);
        delayMicroseconds(3000);
      }

      while (true)
      {
        // Waiting for speed to be set to 0
        int potValue = analogRead(this->potPin);
        if (potValue == 0) {
          break;
        }
      }
      
      break;
    }
    
    if (startCount)
    {
      this->maxSteps += 1;
    }
  }
  digitalWrite(this->dirPin, LOW);

  if (maxSteps > 0)
  {
    this->originalMaxSteps = this->maxSteps;
    return true;
  }
  return false;
}

int FMControllerMethods::getButton1()
{
  return this->button1;
}
int FMControllerMethods::getButton2()
{
  return this->button2;
}
int FMControllerMethods::getButton3()
{
  return this->button3;
}
int FMControllerMethods::getRedPin()
{
  return this->redPin;
}
int FMControllerMethods::getGreenPin()
{
  return this->greenPin;
}
int FMControllerMethods::getBluePin()
{
  return this->bluePin;
}
int FMControllerMethods::getStepPin()
{
  return this->stepPin;
}
int FMControllerMethods::getDirPin()
{
  return this->dirPin;
}
int FMControllerMethods::getLimitBackPin()
{
  return this->limitBackPin;
}
int FMControllerMethods::getLimitFrontPin()
{
  return this->limitFrontPin;
}

int FMControllerMethods::getMaxSteps()
{
  return this->maxSteps;
}
void FMControllerMethods::setMaxSteps(int newMaxSteps)
{
  this->maxSteps = newMaxSteps;
}
int FMControllerMethods::getOriginalMaxSteps()
{
  return this->originalMaxSteps;
}

int FMControllerMethods::getSpeed()
{
  int potValue = analogRead(this->potPin);

  //this->currentPotValue
  // Pot value changes constantly +-30.
  if (potValue == 0 || potValue > (this->currentPotValue + 200) || potValue < (this->currentPotValue - 200))
  {
    this->currentPotValue = potValue;
    Serial.println("New speed is: " + String(this->currentPotValue * 5));
  }

  return this->currentPotValue * 5;
}
